
# Explore gender balance at Western Sydney University


This repository contains the code for [this shiny app](http://137.92.56.94:3838/sageexplore/).

Below are instructions for developers only. 


## Shiny

The sageexplore app is developed with `shiny` in R. Please find extensive documentation here: https://shiny.rstudio.com/.
Pay special attention to [reactivity](https://shiny.rstudio.com/articles/understanding-reactivity.html), which the app depends on heavily and can be harder to understand.

Developing the app further is easiest in Rstudio (since shiny is developed by the Rstudio folks). Although the app normally runs on a server (see below), it can be tested locally in this repository. Simply open `app.R` and click `Run App` (above the script). Note that the download button for the plot only works if you subsequently click 'Open in Browser' (this is a known fault with the Rstudio previewer).



## Hosting

This shiny app is hosted on a virtual machine maintained by Intersect, your point of contact for server-related issues is Gerard Devine (g.devine@westernsydney.edu.au). The url for the app is:
  
```
http://137.92.56.94:3838/sageexplore/
```

Login and password will be emailed to you separately. **All developers share this login and pass, please behave**.

- **Trying to figure out how to login again, some changes to the server have been made. Stay tuned!**
  

## Updating the app
  
The app is under git version control, and the virtual machine simply pulls changes from the same remote repository (https://bitbucket.org/remkoduursma/sageexplore). Make changes locally, then push to that remote. 

Once logged into the server, navigate to the directory where the app is hosted:
  
```
cd /srv/shiny-server/apps/sageexplore/
```

This directory is under version control, so simply pull the changes:
  
```
git pull
```

Note that authentication is via https, which means you don't need the key. Never make changes to the code directly on the server and push.


