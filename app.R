

# Load packages, functions, data.
source("R/preload.R")

# Define user interface.
ui <- fluidPage(theme = shinytheme("flatly"),
   
   titlePanel("SAGE explorer", windowTitle = "SAGE explorer"),
   h4("Explore gender balance for staff & students at Western Sydney University"),
    
   fluidRow(
      column(12, align="center",
             uiOutput("plot1.ui")
      )
   ),
   
   hr(),
   
   fluidRow(
     column(4, 
            selectInput("filter_year", 
                           label=h4("Year:"),
                           choices=as.character(2001:2017),
                           multiple=FALSE,
                           selected="2017",
                           width="30%"),
            
            # Tooltip with help.
            bsTooltip("filter_year", "Select year (only one allowed)",
                      "right", options = list(container = "body")),
            
            uiOutput("select_div"),
            radioButtons("acamgen", label=h4("Position:"),
                         choiceNames=c("Academic","Professional staff"),
                         choiceValues=c("Academic","General"),
                         selected="Academic")
     ),
     column(4,

            uiOutput("select_xvar"),
            radioButtons("include_stud", label=h4("Include students:"),
                         choices=c("Yes","No"),
                         selected = "Yes"),
            
            radioButtons("orderx", label=h4("Order groups by gender balance:"),
                           choices=c("No"=FALSE, "Yes"=TRUE))
              
            ),
     
     column(4,
            radioButtons("plottype", label=h4("Plot type:"),
                         choices=list("Scissor plot"=2,
                                      "Stacked barplot"=1, 
                                      "Grouped barplot"=3)),
            
            # Use custom CSS to place two items side-by-side
            div(style="display:inline-block", 
                numericInput("plotheight", label="Plot height", value=450)),
            div(style="display:inline-block",
                numericInput("plotwidth", label="& width (pixels)", value=450)),
            
            div(style="display:inline-block", radioButtons("plotfiletype", label="Save as file type:",
                         choices=c("png","pdf"), selected="png")),
            div(style="display:inline-block", 
                downloadButton('downloadPlot', 'Save Figure'))
            )
     
     
     
   )

)

# Define server functionality.
server <- function(input, output, session) {
   
  # The plot is a reactive element, so that it fits neatly in the top row.
  # This is necessary because the user can specify the plot width and height.
  plot_height <- reactive(input$plotheight)
  plot_width <- reactive(input$plotwidth)
  
  # The division selector is reactive, because the list of divisions/schools at WSU depend 
  # on the year (i.e. organisation has changed many times over the years).
  output$select_div <- renderUI(selectInput("filter_div",
                                            label=h4("Division / School:"),
                  choices = dplyr::filter(gendat, year == input$filter_year) %>% distinct(division_school) %>% pull,
  multiple=TRUE, width="80%"
  ))
  
  # The X axis (grouping variable) is also reactive because it depends on whether we are plotting
  # academics or professional staff.
  output$select_xvar <- renderUI(radioButtons("groupvar",
                                             label=h4("Grouping variable:"),
                                             width="50%",
                                             choices=if(input$acamgen == "Academic"){
                                                            list("Academic level" = "acam_level",
                                                                 "Division / School" = "division_school")
                                             } else {
                                                            list("HEW" = "HEW",
                                                                 "Division / School" = "division_school"
                                                                 )
                                             }
                                                            ))
  
   # The plotInput function is used by the reactive UI, and sends the parameters to one of the
   # subsidiary functions defined in R/plotting_functions_chrinput.R.
   plotInput <- function(){
       
       # Shiny logic to depend on certain inputs existing, if not don't do anything.
       req(input$filter_year, input$acamgen, input$groupvar)
     
       dat <- filter(gendat, year == input$filter_year)
       
       if(input$include_stud == "No"){
         dat <- filter(dat, position %in% input$acamgen)
       } else {
         dat <- filter(dat, position %in% input$acamgen | staff_student == "student")
       }
       
       if("Academic" %in% input$acamgen){
         dat <- filter(dat, !is.na(acam_level))
       }
       
       if(any(input$filter_div != "")){
         dat <- filter(dat, division_school %in% input$filter_div)
       }
       
       if(input$plottype == 1){
         stacked_barplot(dat, input$groupvar,
                         rotate_xlabs = input$groupvar == "division_school",
                         order=input$orderx)
       }
       if(input$plottype == 2){
         scissor_plot(dat, input$groupvar,
                      rotate_xlabs = input$groupvar == "division_school",
                      order=input$orderx)
       }
       if(input$plottype == 3){
         grouped_barplot(dat, input$groupvar,
                         rotate_xlabs = input$groupvar == "division_school",
                         order=input$orderx)
       }
       
   }
  
   # Make the plot
   output$plot1 <- renderPlot(plotInput())
   
   # Actually draw the plot (this is 'reactive' logic)
   # withSpinner() is from the shinycssloaders package, and displays the loader.
   output$plot1.ui <- renderUI({
     withSpinner(plotOutput("plot1", height=plot_height(), width=plot_width()))
   })
   
   
   # Download button for the plot (special shiny function).
   output$downloadPlot <- downloadHandler(
     
     # sagefigure.png or sagefigure.pdf.
     filename = function(){ sprintf("sagefigure.%s", input$plotfiletype)},
     
     content = function(file) {
       ggsave(file, plot = plotInput(), device=input$plotfiletype)
     }
     
   )
   
}

# Run the application 
shinyApp(ui = ui, server = server)

